Jamulus status webpage
(c) 2021 René Knuvers
license: GPL3

# tl;dr
Use the folder structure to copy files in the correct place. Updracht your systemd (sudo systemctl daemon-reload; then start and enable)
Modify the php-stript to you likings.

# What
This is a web page, based on PHP and some script that is provided on the excellent Jamulus community that shows the current status of the Jamulus online jamming software. You need to have a PHP-enabled webserver running on the jamulus server. The page automatically refreshes every 15 seconds, to limit the load on the server while keeping the information pretty accurate.

Basically bash script that is run frequently by a systemd timer extracts information for each server from the journalctl output. This information includes the running status, max number of users, total number of connected users and the three states recording can be in. The index.php reads that information and shows it in the browser, while adding some prettyfication (tables, logo's, colors).

# How
I have the bash scripts living in /usr/local/bin Make sure they are executable by the systemd-user
The servers are named xxx-jamulus where xxx is a reference to the server name. Be sure to be consistent in the naming in both the systemd services and all the scripts and .dat files
In /var/www/ there are per server two files: jamulus-xxx.dat which is created by the jamulus server using the -m command line option, and xxx-online-state.dat that holds the information extracted from the journal.
In /var/www/html the index.php will live. Set up your server to serve this file (rename index.html if your server takes that as a default over index.php) or rename it to whatever you like it to be.

Happy jamming!
René
