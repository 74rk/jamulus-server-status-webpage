<html>

	<head>
		<title>Jamulus-server </title>
		<?php
		function loadserverstat ( $statfilename )
		{
			$datei = fopen ( $statfilename, "r" );
			while ( !feof ( $datei ) )
			{
				$buffer = fgets ( $datei, 4096 );
				echo $buffer;
			}
			fclose($datei);
		}
		?>

		<meta http-equiv="refresh" content="15">
		
		<style>
			p {
				font-family:	Raleway,Verdana;
				font-size:	12pt;
			}

			a {
				font-family:	Raleway,Verdana;
				font-size:	12pt;
			}

			a:link {
				font-color:	blue;
			}
			
			a:hover {
				font-color:	lightblue;
			}

			a:visited {
				font-color:	darkgray;
			}
			a:active {
				font-color:	hotpink;
			}

			ul {
				font-family:	Raleway,Verdana;
				font-size:	10pt;
				background:	#aaffaa;
				padding:      	3px;
				list-style:	square outside; 
			}
			ul li {
				background:	#ddffdd;
				margin:		2px;
				margin-left:	25px;
				padding:	5px;
			}
			td {
				vertical-align:	top;
			}
		</style>
	</head>
	<body>
		<p style="font-size:24pt; font-weight: bold; color: black">Dit is de Jamulus-server van --- </p>
		<p align=center>
		<a href=https://lounge.jamulus.io/ target="blank"> Jamulus Lounge </a>&emsp;
		<a href=https://explorer.jamulus.io/ target="blank"> Jamulus Explorer </a>
		</p>
		<br>
		<br>
		<table>
		<tr>
			<td align=center width=15%><p style="font-size:14pt; font-weight: bold; font-style: italic; color: black">Publiek</p></td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:middle"><img src = "jamuluslogo.png" width = 80></td>
		</tr>
		<tr>
			<td align = center><p style="font-size:14pt; font-weight: light; font-style: normal; color: black"><?php loadserverstat ( "/var/www/public-online-state.dat" ); ?></p></td>
		</tr>
		<tr>
			<td align = center><p style="font-size:12pt; font-weight: normal; color: black"><?php loadserverstat ( "/var/www/jamulus-public.dat" ); ?></p></td>
		</tr>
		</table>
	</body>

</html>
